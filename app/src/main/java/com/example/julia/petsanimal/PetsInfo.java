package com.example.julia.petsanimal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Julia on 03.05.2018.
 */

public class PetsInfo extends AppCompatActivity {
    TextView nameView;
    TextView breedView;
    TextView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_pets);
        init();

    }

    public void init(){
        nameView = (TextView)findViewById(R.id.name);
        breedView = (TextView)findViewById(R.id.breed);
        imageView = (TextView)findViewById(R.id.image);
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        nameView.setText(name);
        String breed = intent.getStringExtra("breed");
        breedView.setText(breed);
        int image = intent.getIntExtra("image",0);
        imageView.setText(""+image);

    }





}
