package com.example.julia.petsanimal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.example.julia.petsanimal.model.Hourly;
import com.example.julia.petsanimal.model.Weather;
import com.example.julia.petsanimal.model.WeatherData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Julia on 04.05.2018.
 */

public class PageFragment extends Fragment {

    static final String TAG = "myLogs";
    List<Hourly> weather = new ArrayList<>();

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final String SAVE_PAGE_NUMBER = "save_page_number";

    int pageNumber;
    int backColor;
    SharedPreferences sPref;
    DateAdapter adapter;

    static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setInitialData();
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);

        Log.d(TAG, "onCreate: " + pageNumber);

        Random rnd = new Random();
        backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        int savedPageNumber = -1;
        if (savedInstanceState != null) {
            savedPageNumber = savedInstanceState.getInt(SAVE_PAGE_NUMBER);
        }
        Log.d(TAG, "savedPageNumber = " + savedPageNumber);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        adapter = new DateAdapter(view.getContext(), weather, listener);
        recyclerView.setAdapter(adapter);

        TextView tvPage = (TextView) view.findViewById(R.id.tvPage);
        tvPage.setText("Page " + pageNumber);
        tvPage.setBackgroundColor(backColor);
        loadText();

        return view;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_PAGE_NUMBER, pageNumber);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: " + pageNumber);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(v.getContext(),PetsInfo.class).putExtra("name",((Pets)v.getTag()).getName()).
                    putExtra("breed",((Pets)v.getTag()).getBreed()).putExtra("imag",((Pets)v.getTag()).getImage()));
//            Toast.makeText(v.getContext(),((Pets)v.getTag()).getName(),Toast.LENGTH_SHORT).show();

        }
    };



//    private void setInitialData(){
//        for (int i=0; i<9;i++){
//        pet.add(new Pets ("Filik", "Cat", R.drawable.ic_launcher));}
//
//    }
    private void loadText (){
        sPref = this.getActivity().getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString(MainActivity.SAVED_TEXT, "");

        if(savedText.length()>0) {
            Gson gson = new Gson();
            WeatherData weatherData = gson.fromJson(savedText, WeatherData.class);
//            weather.addAll(weatherData.getData().getWeather().get(0).getHourly());
//            adapter.notifyDataSetChanged();
            Toast.makeText(this.getContext(), weatherData.getData().getWeather().size() + "", Toast.LENGTH_SHORT).show();
        }
    }
}
