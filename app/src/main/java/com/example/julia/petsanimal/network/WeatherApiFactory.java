package com.example.julia.petsanimal.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherApiFactory {


    public static WeatherApi service;
    private static Gson gsonInstance;


    public static WeatherApi getWeatherApi() {

        String BASE_URL = "http://api.worldweatheronline.com";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(buildGsonConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        service = retrofit.create(WeatherApi.class);

        return service;

    }

    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        gsonInstance = gsonBuilder.create();

        return GsonConverterFactory.create(gsonInstance);
    }
}
