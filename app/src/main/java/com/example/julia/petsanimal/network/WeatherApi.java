package com.example.julia.petsanimal.network;

import com.example.julia.petsanimal.model.WeatherData;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

public interface WeatherApi {

    @GET("/premium/v1/weather.ashx?key=591a209d1eb2418a80794822180405&q=London&format=json&num_of_days=3")
    Observable<Response<WeatherData>> getWeatherService();


}