package com.example.julia.petsanimal;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.julia.petsanimal.model.Hourly;
import com.example.julia.petsanimal.model.Weather;

import java.util.List;
/**
 * Created by Julia on 03.05.2018.
 */

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<Hourly> weather;
    private View.OnClickListener listener;

    DateAdapter(Context context, List<Hourly> weather, View.OnClickListener listener) {
        this.weather = weather;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }
    @Override
    public DateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.pets_item, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);

    }
    @Override
    public void onBindViewHolder(DateAdapter.ViewHolder holder, int position) {
        Hourly hourly = weather.get(position);
//        holder.imageView.setImageResource(hourly.getImage());
        holder.nameView.setText(hourly.getTempC());
//        holder.breedView.setText(hourly.getWindspeedKmph());
//        holder.itemView.setTag(hourly);

    }
    @Override
    public int getItemCount() {
        return weather.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageView;
        final TextView nameView, breedView;
        ViewHolder(View view){
            super(view);
            imageView = (ImageView)view.findViewById(R.id.image);
            nameView = (TextView) view.findViewById(R.id.name);
            breedView = (TextView) view.findViewById(R.id.breed);
        }
    }
}
