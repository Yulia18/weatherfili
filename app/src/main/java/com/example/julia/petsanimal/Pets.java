package com.example.julia.petsanimal;


/**
 * Created by Julia on 03.05.2018.
 */

public class Pets {
    private String breed;
    private String name;
    private int image;

    public Pets(String breed, String name, int image) {
        this.breed = breed;
        this.name = name;
        this.image = image;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
