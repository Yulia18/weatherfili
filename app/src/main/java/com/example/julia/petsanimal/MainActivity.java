package com.example.julia.petsanimal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import com.example.julia.petsanimal.model.WeatherData;
import com.example.julia.petsanimal.network.WeatherApiFactory;
import com.google.gson.Gson;


import retrofit2.Response;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
;


public class MainActivity extends AppCompatActivity {

    List<Pets> pet = new ArrayList<>();
    static final String TAG = "myLogs";
    static final int PAGE_COUNT = 10;
    public static final String SAVED_TEXT = "saved_text";

    ViewPager pager;
    PagerAdapter pagerAdapter;
    SharedPreferences sPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        getWeather();
    }
    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }

    private void getWeather(){
          WeatherApiFactory.getWeatherApi().getWeatherService()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<Response<WeatherData>>() {
                               @Override
                               public void call(Response<WeatherData> weatherDataResponse) {
                                   Gson gson = new Gson();
                                   String textTo = gson.toJson(weatherDataResponse);
                                   sPref = getPreferences(MODE_PRIVATE);
                                   SharedPreferences.Editor ed = sPref.edit();
                                   ed.putString(SAVED_TEXT, textTo);
                                   ed.commit();
                                   Toast.makeText(MainActivity.this, "" + weatherDataResponse.body()
                                           .getData().getWeather().size() + "", Toast.LENGTH_LONG).show();
                               }
                           }

                );
    }




}
